/* Теоретичні питання.
1. Для створення DOM елементів можна використовувати 2 способа - "document.createElement("tag")" та "document.createTextNode(text)".
Перший спосіб створює новий вузол елемента, в ньому вказується тег. Другим способом створюється вузол тексту із вказаним в
дужках текстом.
Після створення DOM елемента потрібно його вставити в документ. Для додавання елементів в документ існує декілька методів:
 - "append()" додає в кінець вузла вузол або рядок;
 - "prepend()" додає вузол або рядок на початок вузла;
 - "after()" вставляє вузол або рядок після існуючого вузла, до якого ми використовуємо цей метод;
 - "before()" вставляє вузол, рядок перед існуючим вузлом;
 - "replaceWith()" замінює існуючий вузол новим рядком або вузлом.
 Також існує ще один універсальний метод - "insertAdjacentHTML(where, HTML)". В дужках першим параметром прописується куди треба
 вставити елемент ("beforebegin" – вставити html безпосередньо перед вузлом,"afterbegin" – вставити html в вузол на початку,
"beforeend" – вставити html в вузол в кінці, "afterend" – вставити html безпосередньо після вузла. Наприклад:
     ul.insertAdjacentHTML("beforeend", "<li>Text content</li>");

2. Для видалення елемента з документа використовується метод remove(). Наприклад нам потрібно видалити елемент з класом
"navigation". Для цього ми повинні знайти елемент з цим классом:
       const del = document.querySelector(".navigation");
       видалити цей елемент в документі, але в консолі він буде отображатись:
       del.remove();
  Також можна використати видалення через батьківський елемент за допомогою метода "removeChild()":
       const par = getElementById("parent");
       const child = document.querySelector(".navigation");
       видаляємо дочірній елемент з класом "navigation" вузла з id "parent":
       par.removeChild(child);
       щоб видалити елемент не тільки з документу, а з js, ми присвоюємо текстовому контенту батьківського елемента
       пустий рядок:
       par.textContent = "";
3. Для вставки DOM-елементів перед/після іншого DOM-елемента використовуються 2 метода "before()" та "after()". Також
можно використати метод "insertAdjacentElement(beforebegin, elem)" - перед DOM-елементом або "insertAdjacentElement(afterend, elem)" -
після DOM-елемента. Перші методи більш сучасні, та легше прописуються в коді.
*/

'use strict'
const link = document.createElement('a');
link.setAttribute('href', '#');
link.textContent = 'Learn More';
const footer = document.querySelector('footer');
footer.append(link);

const list = document.createElement('select');
list.id = 'rating';
const main = document.querySelector('main');
main.prepend(list);


let fragment = document.createDocumentFragment();
for(let i = 4; i >= 1; i--){
    let option;
    if(i === 4){
        option = new Option('4 Stars', '4');
    } else if(i === 3) {
        option = new Option('3 Stars', '3');
    } else if(i === 2){
        option = new Option('2 Stars', '2');
    } else {
        option = new Option('1 Star', '1');
    }
    fragment.append(option);
}

list.append(fragment);

























